const express = require('express');

const {
    getAllPost,
    createPost,
    getPostById,
    updatePostById,
    deletePostById,
    getPostsOfUser
} = require("../controller/postController");

const postRouter = express.Router();

postRouter.get("/post",getAllPost);
postRouter.post("/post",createPost);
postRouter.get("/post/:postId", getPostById);
postRouter.get("/user/:userId/post", getPostsOfUser);
postRouter.put("/post/:postId", updatePostById);
postRouter.delete("/post/:postId", deletePostById);

module.exports = { postRouter };