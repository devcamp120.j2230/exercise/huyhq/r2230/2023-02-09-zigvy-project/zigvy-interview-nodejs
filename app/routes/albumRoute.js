const express = require('express');

const {
    getAllAlbum,
    createAlbum,
    getAlbumById,
    updateAlbumById,
    deleteAlbumById,
    getAlbumsOfUser
} = require("../controller/albumController");

const albumRouter = express.Router();

albumRouter.get("/album",getAllAlbum);
albumRouter.post("/album",createAlbum);
albumRouter.get("/album/:albumId", getAlbumById);
albumRouter.get("/user/:userId/album", getAlbumsOfUser);
albumRouter.put("/album/:albumId", updateAlbumById);
albumRouter.delete("/album/:albumId", deleteAlbumById);

module.exports = { albumRouter };