const express = require('express');

const {
    getAllComment,
    createComment,
    getCommentById,
    updateCommentById,
    deleteCommentById,
    getCommentsOfPost
} = require("../controller/commentController");

const commentRouter = express.Router();

commentRouter.get("/comment",getAllComment);
commentRouter.post("/comment",createComment);
commentRouter.get("/comment/:commentId", getCommentById);
commentRouter.get("/post/:postId/comment", getCommentsOfPost);
commentRouter.put("/comment/:commentId", updateCommentById);
commentRouter.delete("/comment/:commentId", deleteCommentById);

module.exports = { commentRouter };