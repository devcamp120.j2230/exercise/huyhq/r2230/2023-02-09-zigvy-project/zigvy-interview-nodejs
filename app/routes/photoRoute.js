const express = require('express');

const {
    getAllPhoto,
    createPhoto,
    getPhotoById,
    updatePhotoById,
    deletePhotoById,
    getPhotosOfUser
} = require("../controller/photoController");

const photoRouter = express.Router();

photoRouter.get("/photo",getAllPhoto);
photoRouter.post("/photo",createPhoto);
photoRouter.get("/photo/:photoId", getPhotoById);
photoRouter.get("/album/:albumId/photo", getPhotosOfUser);
photoRouter.put("/photo/:photoId", updatePhotoById);
photoRouter.delete("/photo/:photoId", deletePhotoById);

module.exports = { photoRouter };