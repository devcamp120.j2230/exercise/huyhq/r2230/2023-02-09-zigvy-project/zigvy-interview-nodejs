const express = require('express');

const {
    getAllTodo,
    createTodo,
    getTodoById,
    updateTodoById,
    deleteTodoById,
    getTodosOfUser
} = require("../controller/todoController");

const todoRouter = express.Router();

todoRouter.get("/todo",getAllTodo);
todoRouter.post("/todo",createTodo);
todoRouter.get("/todo/:todoId", getTodoById);
todoRouter.get("/user/:userId/todo", getTodosOfUser);
todoRouter.put("/todo/:todoId", updateTodoById);
todoRouter.delete("/todo/:todoId", deleteTodoById);

module.exports = { todoRouter };