const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const photoSchema = new Schema({
    _id: mongoose.Types.ObjectId,

    albumId:{
        type: mongoose.Types.ObjectId,
        ref: "user",
        require: true
    },
    title:{
        type: String,
        require: true,
        unique: true
    },
    
    url: {
        type: String,
        require: true
    },
    thumbnailUrl: {
        type: String,
        require: true
    }
});

 module.exports = mongoose.model("photo", photoSchema);
