const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const commentSchema = new Schema({
    _id: mongoose.Types.ObjectId,

    postId:{
        type: mongoose.Types.ObjectId,
        ref: "post",
        require: true
    },
    email:{
        type: String,
        require: true,
        unique: true
    },
    name: {
        type: String,
        require: true
    },
    body: {
        type: String,
        require: true
    },
});

 module.exports = mongoose.model("comment", commentSchema);
