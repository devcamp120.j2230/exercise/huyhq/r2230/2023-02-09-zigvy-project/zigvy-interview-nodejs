const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const postSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    userId:{
        type: mongoose.Types.ObjectId,
        ref: 'user',
        require: true
    },
    title: {
        type: String,
        require: true
    },
    body: {
        type: String,
        require: true
    }
});

module.exports = mongoose.model("post", postSchema);