const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const albumSchema = new Schema({
    _id: mongoose.Types.ObjectId,

    userId:{
        type: mongoose.Types.ObjectId,
        ref: "user",
        require: true
    },
    title:{
        type: String,
        require: true,
        unique: true
    },
});

 module.exports = mongoose.model("album", albumSchema);
