const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const todoSchema = new Schema({
    _id: mongoose.Types.ObjectId,

    userId:{
        type: mongoose.Types.ObjectId,
        ref: "user",
        require: true
    },
    title:{
        type: String,
        require: true,
        unique: true
    },
    
    complete:{
        type: Boolean
    }
});

 module.exports = mongoose.model("todo", todoSchema);
