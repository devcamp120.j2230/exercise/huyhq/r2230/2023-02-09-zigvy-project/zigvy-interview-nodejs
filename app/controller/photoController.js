const {default: mongoose} = require('mongoose');
const photoModel = require('../model/photoModel');
const albumModel = require('../model/albumModel');

const createPhoto = (req, res)=>{
    let body = req.body;
    if(!body.albumId){
        return res.status(400).json({
            status: "Error 400",
            message: "Album Id is required!"
        })
    };

    if(!mongoose.Types.ObjectId.isValid(body.albumId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Album Id invalid!`
        })
    };
    if(!body.title){
        return res.status(400).json({
            status: "Error 400",
            message: "Title is required!"
        })
    };
    if(!body.url){
        return res.status(400).json({
            status: "Error 400",
            message: "Url is required!"
        })
    };
    if(!body.thumbnailUrl){
        return res.status(400).json({
            status: "Error 400",
            message: "Thumbnail Url is required!"
        })
    };

    const newPhoto = new photoModel({
        _id: mongoose.Types.ObjectId(),
        albumId: body.albumId,
        title: body.title,
        url: body.url,
        thumbnailUrl: body.thumbnailUrl
    })

    photoModel.create(newPhoto, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(201).json({
                status: 'Create new data successfull!',
                data
            })
        }
    })
};

const getAllPhoto = (req, res)=>{
    let albumId = req.query.albumId

    let condition = {};

    if(albumId){
        condition.albumId = albumId
    };

    photoModel.find(condition)
    .exec((error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

const getPhotoById = (req, res)=>{
    let photoId = req.params.photoId;
    if(!mongoose.Types.ObjectId.isValid(photoId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Photo Id invalid!`
        })
    }
    photoModel.findById(photoId, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

const updatePhotoById = (req, res)=>{
    let photoId = req.params.photoId;
    if(!mongoose.Types.ObjectId.isValid(photoId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Photo Id invalid!`
        })
    };

    let body = req.body;
    if(!body.albumId){
        return res.status(400).json({
            status: "Error 400",
            message: "Album Id is required!"
        })
    };

    if(!mongoose.Types.ObjectId.isValid(body.albumId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Album Id invalid!`
        })
    };
    if(!body.title){
        return res.status(400).json({
            status: "Error 400",
            message: "Title is required!"
        })
    };
    if(!body.url){
        return res.status(400).json({
            status: "Error 400",
            message: "Url is required!"
        })
    };
    if(!body.thumbnailUrl){
        return res.status(400).json({
            status: "Error 400",
            message: "Thumbnail Url is required!"
        })
    };

    const photo = new photoModel({
        albumId: body.albumId,
        title: body.title,
        url: body.url,
        thumbnailUrl: body.thumbnailUrl
    })

    photoModel.findByIdAndUpdate(photoId, photo,(error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Update data successfull!',
                data
            })
        }
    })
};

const deletePhotoById = (req, res)=>{
    let photoId = req.params.photoId;
    if(!mongoose.Types.ObjectId.isValid(photoId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Photo Id invalid!`
        })
    };

    photoModel.findByIdAndDelete(photoId, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(204).json({
                status: 'Delete data successfull!',
                data
            })
        }
    })
};

const getPhotosOfUser = (req, res)=>{
    let albumId = req.params.albumId;
    if(!mongoose.Types.ObjectId.isValid(albumId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Album Id invalid!`
        })
    };
    let condition = {};
    if(albumId){
        condition.albumId = albumId
    };

    photoModel.find(condition)
    .exec((error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

module.exports = {
    getAllPhoto,
    createPhoto,
    getPhotoById,
    updatePhotoById,
    deletePhotoById,
    getPhotosOfUser
}