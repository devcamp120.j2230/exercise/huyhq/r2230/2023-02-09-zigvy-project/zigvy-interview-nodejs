const {default: mongoose} = require('mongoose');
const postModel = require('../model/postModel');
const userModel = require('../model/userModel');

const createPost = (req, res)=>{
    let body = req.body;
    if(!body.userId){
        return res.status(400).json({
            status: "Error 400",
            message: "User Id is required!"
        })
    };

    if(!mongoose.Types.ObjectId.isValid(body.userId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `User Id invalid!`
        })
    };
    if(!body.title){
        return res.status(400).json({
            status: "Error 400",
            message: "Title is required!"
        })
    };
    if(!body.body){
        return res.status(400).json({
            status: "Error 400",
            message: "Post is required!"
        })
    };

    const newPost = new postModel({
        _id: mongoose.Types.ObjectId(),
        userId: body.userId,
        title: body.title,
        body: body.body
    })

    postModel.create(newPost, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(201).json({
                status: 'Create new data successfull!',
                data
            })
        }
    })
};

const getAllPost = (req, res)=>{
    let userId = req.query.userId
    let condition = {};
    if(userId){
        condition.userId = userId
    };
    postModel.find(condition)
    .exec((error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

const getPostById = (req, res)=>{
    let postId = req.params.postId;
    if(!mongoose.Types.ObjectId.isValid(postId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Post Id invalid!`
        })
    }
    postModel.findById(postId, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

const updatePostById = (req, res)=>{
    let postId = req.params.postId;
    if(!mongoose.Types.ObjectId.isValid(postId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Post Id invalid!`
        })
    };

    let body = req.body;
    if(!body.userId){
        return res.status(400).json({
            status: "Error 400",
            message: "User Id is required!"
        })
    };

    if(!mongoose.Types.ObjectId.isValid(body.userId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `User Id invalid!`
        })
    };
    if(!body.title){
        return res.status(400).json({
            status: "Error 400",
            message: "Title is required!"
        })
    };
    if(!body.body){
        return res.status(400).json({
            status: "Error 400",
            message: "Post is required!"
        })
    };

    const post = new postModel({
        userId: body.userId,
        title: body.title,
        body: body.body
    })

    postModel.findByIdAndUpdate(postId, post,(error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Update data successfull!',
                data
            })
        }
    })
};

const deletePostById = (req, res)=>{
    let postId = req.params.postId;
    if(!mongoose.Types.ObjectId.isValid(postId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Post Id invalid!`
        })
    };

    postModel.findByIdAndDelete(postId, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(204).json({
                status: 'Delete data successfull!',
                data
            })
        }
    })
};

const getPostsOfUser = (req, res)=>{
    let userId = req.params.userId;
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `User Id invalid!`
        })
    };
    let condition = {};
    if(userId){
        condition.userId = userId
    };

    postModel.find(condition)
    .exec((error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

module.exports = {
    getAllPost,
    createPost,
    getPostById,
    updatePostById,
    deletePostById,
    getPostsOfUser
}