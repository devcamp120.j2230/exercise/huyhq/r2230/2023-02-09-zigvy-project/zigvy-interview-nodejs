const {default: mongoose} = require('mongoose');
const userModel = require('../model/userModel');

const createUser = (req, res)=>{
    let body = req.body;
    if(!body.name){
        return res.status(400).json({
            status: "Error 400",
            message: "Name is required!"
        })
    };

    if(!body.username){
        return res.status(400).json({
            status: "Error 400",
            message: "Username is required!"
        })
    };

    if(!body.address.street){
        return res.status(400).json({
            status: "Error 400",
            message: "Street is required!"
        })
    };

    if(!body.address.street){
        return res.status(400).json({
            status: "Error 400",
            message: "Street is required!"
        })
    };
    if(!body.address.suite){
        return res.status(400).json({
            status: "Error 400",
            message: "Suite is required!"
        })
    };
    if(!body.address.city){
        return res.status(400).json({
            status: "Error 400",
            message: "City is required!"
        })
    };
    if(!body.address.zipcode){
        return res.status(400).json({
            status: "Error 400",
            message: "Zipcode is required!"
        })
    };

    if(!body.address.geo.lat){
        return res.status(400).json({
            status: "Error 400",
            message: "Lat is required!"
        })
    };

    if(!body.address.geo.lng){
        return res.status(400).json({
            status: "Error 400",
            message: "Leo is required!"
        })
    };

    if(!body.phone){
        return res.status(400).json({
            status: "Error 400",
            message: "Phone is required!"
        })
    };

    if(!body.website){
        return res.status(400).json({
            status: "Error 400",
            message: "Website is required!"
        })
    };

    if(!body.company.name){
        return res.status(400).json({
            status: "Error 400",
            message: "Company name is required!"
        })
    };

    if(!body.company.catchPhrase){
        return res.status(400).json({
            status: "Error 400",
            message: "Company CatchPhrase is required!"
        })
    };

    if(!body.company.bs){
        return res.status(400).json({
            status: "Error 400",
            message: "Company Bs is required!"
        })
    };

    const newUser = new userModel({
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        username: body.username,
        address: {
            street:  body.address.street,
            suite:  body.address.suite,
            city:  body.address.city,
            zipcode:  body.address.zipcode,
            geo: {
                lat: body.address.geo.lat,
                lng: body.address.geo.lng
            }
        },
        phone: body.phone,
        website: body.website,
        company: {
            name: body.company.name,
            catchPhrase: body.company.catchPhrase,
            bs: body.company.bs
        }
    })

    userModel.create(newUser, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(201).json({
                status: 'Create new data successfull!',
                data
            })
        }
    })
};

const getAllUser = (req, res)=>{
    userModel.find((error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

const getUserById = (req, res)=>{
    let userId = req.params.userId;
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `User Id invalid!`
        })
    }
    userModel.findById(userId, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

const updateUserById = (req, res)=>{
    let userId = req.params.userId;
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `User Id invalid!`
        })
    };

    let body = req.body;
    if(!body.name){
        return res.status(400).json({
            status: "Error 400",
            message: "Name is required!"
        })
    };

    if(!body.username){
        return res.status(400).json({
            status: "Error 400",
            message: "Username is required!"
        })
    };

    if(!body.address.street){
        return res.status(400).json({
            status: "Error 400",
            message: "Street is required!"
        })
    };

    if(!body.address.street){
        return res.status(400).json({
            status: "Error 400",
            message: "Street is required!"
        })
    };
    if(!body.address.suite){
        return res.status(400).json({
            status: "Error 400",
            message: "Suite is required!"
        })
    };
    if(!body.address.city){
        return res.status(400).json({
            status: "Error 400",
            message: "City is required!"
        })
    };
    if(!body.address.zipcode){
        return res.status(400).json({
            status: "Error 400",
            message: "Zipcode is required!"
        })
    };

    if(!body.address.geo.lat){
        return res.status(400).json({
            status: "Error 400",
            message: "Lat is required!"
        })
    };

    if(!body.address.geo.lng){
        return res.status(400).json({
            status: "Error 400",
            message: "Leo is required!"
        })
    };

    if(!body.phone){
        return res.status(400).json({
            status: "Error 400",
            message: "Phone is required!"
        })
    };

    if(!body.website){
        return res.status(400).json({
            status: "Error 400",
            message: "Website is required!"
        })
    };

    if(!body.company.name){
        return res.status(400).json({
            status: "Error 400",
            message: "Company name is required!"
        })
    };

    if(!body.company.catchPhrase){
        return res.status(400).json({
            status: "Error 400",
            message: "Company CatchPhrase is required!"
        })
    };

    if(!body.company.bs){
        return res.status(400).json({
            status: "Error 400",
            message: "Company Bs is required!"
        })
    };
    const user = new userModel({
        name: body.name,
        username: body.username,
        address: {
            street:  body.address.street,
            suite:  body.address.suite,
            city:  body.address.city,
            zipcode:  body.address.zipcode,
            geo: {
                lat: body.address.geo.lat,
                leo: body.address.geo.leo
            }
        },
        phone: body.phone,
        website: body.website,
        company: {
            name: body.company.name,
            catchPhrase: body.company.catchPhrase,
            bs: body.company.bs
        }
    });

    userModel.findByIdAndUpdate(userId, user,(error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Update data successfull!',
                data
            })
        }
    })
};

const deleteUserById = (req, res)=>{
    let userId = req.params.userId;
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `User Id invalid!`
        })
    };

    userModel.findByIdAndDelete(userId, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(204).json({
                status: 'Delete data successfull!',
                data
            })
        }
    })
};

module.exports = {
    getAllUser,
    createUser,
    getUserById,
    updateUserById,
    deleteUserById
}