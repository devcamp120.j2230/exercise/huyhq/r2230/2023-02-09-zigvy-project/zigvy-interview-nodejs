const {default: mongoose} = require('mongoose');
const albumModel = require('../model/albumModel');
const userModel = require('../model/userModel');

const createAlbum = (req, res)=>{
    let body = req.body;
    if(!body.userId){
        return res.status(400).json({
            status: "Error 400",
            message: "User Id is required!"
        })
    };

    if(!mongoose.Types.ObjectId.isValid(body.userId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `User Id invalid!`
        })
    };
    if(!body.title){
        return res.status(400).json({
            status: "Error 400",
            message: "Title is required!"
        })
    };

    const newAlbum = new albumModel({
        _id: mongoose.Types.ObjectId(),
        userId: body.userId,
        title: body.title,
    })

    albumModel.create(newAlbum, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(201).json({
                status: 'Create new data successfull!',
                data
            })
        }
    })
};

const getAllAlbum = (req, res)=>{
    let userId = req.query.userId

    let condition = {};

    if(userId){
        condition.userId = userId
    };

    albumModel.find(condition)
    .exec((error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

const getAlbumById = (req, res)=>{
    let albumId = req.params.albumId;
    if(!mongoose.Types.ObjectId.isValid(albumId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Album Id invalid!`
        })
    }
    albumModel.findById(albumId, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

const updateAlbumById = (req, res)=>{
    let albumId = req.params.albumId;
    if(!mongoose.Types.ObjectId.isValid(albumId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Album Id invalid!`
        })
    };

    let body = req.body;
    if(!body.userId){
        return res.status(400).json({
            status: "Error 400",
            message: "User Id is required!"
        })
    };

    if(!mongoose.Types.ObjectId.isValid(body.userId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `User Id invalid!`
        })
    };
    if(!body.title){
        return res.status(400).json({
            status: "Error 400",
            message: "Title is required!"
        })
    };

    const album = new albumModel({
        userId: body.userId,
        title: body.title,
    })

    albumModel.findByIdAndUpdate(albumId, album,(error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Update data successfull!',
                data
            })
        }
    })
};

const deleteAlbumById = (req, res)=>{
    let albumId = req.params.albumId;
    if(!mongoose.Types.ObjectId.isValid(albumId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Album Id invalid!`
        })
    };

    albumModel.findByIdAndDelete(albumId, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(204).json({
                status: 'Delete data successfull!',
                data
            })
        }
    })
};

const getAlbumsOfUser = (req, res)=>{
    let userId = req.params.userId;
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `User Id invalid!`
        })
    };
    let condition = {};
    if(userId){
        condition.userId = userId
    };

    albumModel.find(condition)
    .exec((error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

module.exports = {
    getAllAlbum,
    createAlbum,
    getAlbumById,
    updateAlbumById,
    deleteAlbumById,
    getAlbumsOfUser
}