const {default: mongoose} = require('mongoose');
const postModel = require('../model/postModel');
const commentModel = require('../model/commentModel');

const createComment = (req, res)=>{
    let body = req.body;
    if(!body.postId){
        return res.status(400).json({
            status: "Error 400",
            message: "Post Id is required!"
        })
    };

    if(!mongoose.Types.ObjectId.isValid(body.postId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Post Id invalid!`
        })
    };

    if(!body.email){
        return res.status(400).json({
            status: "Error 400",
            message: "Email is required!"
        })
    };

    if(!body.name){
        return res.status(400).json({
            status: "Error 400",
            message: "Name is required!"
        })
    };

    if(!body.body){
        return res.status(400).json({
            status: "Error 400",
            message: "Post is required!"
        })
    };

    const newComment = new commentModel({
        _id: mongoose.Types.ObjectId(),
        postId:body.postId,
        email:body.email,
        name: body.name,
        body: body.body,
    })

    commentModel.create(newComment, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(201).json({
                status: 'Create new data successfull!',
                data
            })
        }
    })
};

const getAllComment = (req, res)=>{
    let postId = req.query.postId

    let condition = {};

    if(postId){
        condition.postId = postId
    };

    commentModel.find(condition)
    .exec((error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

const getCommentById = (req, res)=>{
    let commentId = req.params.commentId;
    if(!mongoose.Types.ObjectId.isValid(commentId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Comment Id invalid!`
        })
    }
    commentModel.findById(commentId, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

const updateCommentById = (req, res)=>{
    let commentId = req.params.commentId;
    if(!mongoose.Types.ObjectId.isValid(commentId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Comment Id invalid!`
        })
    };

    let body = req.body;
    if(!body.postId){
        return res.status(400).json({
            status: "Error 400",
            message: "Post Id is required!"
        })
    };

    if(!mongoose.Types.ObjectId.isValid(body.postId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Post Id invalid!`
        })
    };

    if(!body.email){
        return res.status(400).json({
            status: "Error 400",
            message: "Email is required!"
        })
    };

    if(!body.name){
        return res.status(400).json({
            status: "Error 400",
            message: "Name is required!"
        })
    };

    if(!body.body){
        return res.status(400).json({
            status: "Error 400",
            message: "Post is required!"
        })
    };

    const comment = new commentModel({
        postId:body.postId,
        email:body.email,
        name: body.name,
        body: body.body,
    })

    commentModel.findByIdAndUpdate(commentId, comment,(error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Update data successfull!',
                data
            })
        }
    })
};

const deleteCommentById = (req, res)=>{
    let commentId = req.params.commentId;
    if(!mongoose.Types.ObjectId.isValid(commentId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Comment Id invalid!`
        })
    };

    commentModel.findByIdAndDelete(commentId, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(204).json({
                status: 'Delete data successfull!',
                data
            })
        }
    })
};

const getCommentsOfPost = (req, res)=>{
    let postId = req.params.postId;
    if(!mongoose.Types.ObjectId.isValid(postId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Post Id invalid!`
        })
    };
    let condition = {};
    if(postId){
        condition.postId = postId
    };

    commentModel.find(condition)
    .exec((error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

module.exports = {
    getAllComment,
    createComment,
    getCommentById,
    updateCommentById,
    deleteCommentById,
    getCommentsOfPost
}