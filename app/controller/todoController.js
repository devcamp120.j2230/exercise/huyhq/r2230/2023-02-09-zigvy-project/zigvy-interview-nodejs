const {default: mongoose} = require('mongoose');
const todoModel = require('../model/todoModel');
const userModel = require('../model/userModel');

const createTodo = (req, res)=>{
    let body = req.body;
    if(!body.userId){
        return res.status(400).json({
            status: "Error 400",
            message: "User Id is required!"
        })
    };

    if(!mongoose.Types.ObjectId.isValid(body.userId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `User Id invalid!`
        })
    };
    if(!body.title){
        return res.status(400).json({
            status: "Error 400",
            message: "Title is required!"
        })
    };

    const newTodo = new todoModel({
        _id: mongoose.Types.ObjectId(),
        userId: body.userId,
        title: body.title,
        complete: body.complete
    })

    todoModel.create(newTodo, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(201).json({
                status: 'Create new data successfull!',
                data
            })
        }
    })
};

const getAllTodo = (req, res)=>{
    let userId = req.query.userId

    let condition = {};

    if(userId){
        condition.userId = userId
    };

    todoModel.find(condition)
    .exec((error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

const getTodoById = (req, res)=>{
    let todoId = req.params.todoId;
    if(!mongoose.Types.ObjectId.isValid(todoId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Todo Id invalid!`
        })
    }
    todoModel.findById(todoId, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

const updateTodoById = (req, res)=>{
    let todoId = req.params.todoId;
    if(!mongoose.Types.ObjectId.isValid(todoId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Todo Id invalid!`
        })
    };

    let body = req.body;
    if(!body.userId){
        return res.status(400).json({
            status: "Error 400",
            message: "User Id is required!"
        })
    };

    if(!mongoose.Types.ObjectId.isValid(body.userId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `User Id invalid!`
        })
    };
    if(!body.title){
        return res.status(400).json({
            status: "Error 400",
            message: "Title is required!"
        })
    };

    const todo = new todoModel({
        userId: body.userId,
        title: body.title,
        complete: body.complete
    })

    todoModel.findByIdAndUpdate(todoId, todo,(error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Update data successfull!',
                data
            })
        }
    })
};

const deleteTodoById = (req, res)=>{
    let todoId = req.params.todoId;
    if(!mongoose.Types.ObjectId.isValid(todoId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `Todo Id invalid!`
        })
    };

    todoModel.findByIdAndDelete(todoId, (error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(204).json({
                status: 'Delete data successfull!',
                data
            })
        }
    })
};

const getTodosOfUser = (req, res)=>{
    let userId = req.params.userId;
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(500).json({
            status: `Error 500`,
            message: `User Id invalid!`
        })
    };
    let condition = {};
    if(userId){
        condition.userId = userId
    };

    todoModel.find(condition)
    .exec((error, data)=>{
        if(error){
            return res.status(500).json({
                status: `Error 500: ${error.message}`
            })
        }else{
            return res.status(200).json({
                status: 'Get data successfull!',
                data
            })
        }
    })
};

module.exports = {
    getAllTodo,
    createTodo,
    getTodoById,
    updateTodoById,
    deleteTodoById,
    getTodosOfUser
}