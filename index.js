const express = require("express");
const mongoose = require("mongoose");

const app = express();
app.use(express.json());

//get model
const userModel = require("./app/model/userModel");
const postModel = require("./app/model/postModel");
const commentModel = require("./app/model/commentModel");
const albumModel = require("./app/model/albumModel");
const photoModel = require("./app/model/photoModel");
const todoModel = require("./app/model/todoModel");
//get route
const {userRouter} = require("./app/routes/userRoute");
const {postRouter} = require("./app/routes/postRoute");
const {commentRouter} = require("./app/routes/commentRoute");
const {albumRouter} = require("./app/routes/albumRoute");
const {photoRouter} = require("./app/routes/photoRoute");
const {todoRouter} = require("./app/routes/todoRoute");

const port = 8000;

const dbName = "CRUD_Zigvy";
mongoose.connect("mongodb://localhost:27017/"+dbName, (err)=>{
    if(err) throw err;
    console.log("Connect Db: "+dbName);
});

app.use("/", userRouter);
app.use("/", postRouter);
app.use("/", commentRouter);
app.use("/", albumRouter);
app.use("/", photoRouter);
app.use("/", todoRouter);

app.listen(port, ()=>{
    console.log("Connect to port: "+port);
})